# arduino-cli


## arduino-cli core

```bash
arduino-cli core upgrade
arduino-cli core update-index
arduino-cli core install esp32
arduino-cli core install esp32:esp32
arduino-cli core list
arduino-cli core install doit
arduino-cli core install esp32:esp32:doit
arduino-cli core listall
arduino-cli core install esp32:esp32:nodemcu-32s
```

## arduino-cli board

```bash
arduino-cli board list
arduino-cli board listall | grep 'nano'
arduino-cli board listall | grep 's2'
arduino-cli board listall | grep 'saola'
arduino-cli board listall
arduino-cli board listall esp32 | grep 'wrover'
arduino-cli board search 'esp32-s2'
arduino-cli board search 'saola'
arduino-cli board help
arduino-cli board listall | grep 'nodemcu'
arduino-cli board listall esp32 | grep 'rover'
arduino-cli board listall esp32 | grep 's2'
arduino-cli board listall esp32 | grep 'w10'
arduino-cli board listall esp32 | grep 'saola'
arduino-cli board listall esp32
arduino-cli board details esp32:esp32:nodemcu-32s
```
## arduino-cli config

```bash
arduino-cli config init
arduino-cli config set library.enable_unsafe_install true
arduino-cli config set board_manager.additional_urls "https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json"
arduino-cli config set board_manager.additional_urls "https://espressif.github.io/arduino-esp32/package_esp32_index.json"
```

## arduino-cli compile

```bash
arduino-cli compile . --fqbn 'espressif:esp32:esp32s2'
arduino-cli compile . --fqbn 'arduino:avr:nano'
arduino-cli compile . --fqbn 'esp32:esp32:esp32wrover'
arduino-cli compile . --fqbn 'esp32:esp32:esp32doit-devkit-v1'
arduino-cli compile . --fqbn 'esp32:esp32:esp32'
arduino-cli compile . --fqbn 'esp32:esp32'
arduino-cli compile . --fqbn 'esp32:esp32:nodemcu-32s'
arduino-cli compile --fqbn esp32:esp32:nodemcu-32s .
arduino-cli compile --fqbn esp32:esp32:nodemcu-32s blink.ino
arduino-cli compile --fqbn esp32:esp32:nodemcu-32s blink
```

## arduino-cli upload

```bash
arduino-cli upload . -p /dev/ttyUSB0 --fqbn 'esp32:esp32:esp32s2'
arduino-cli upload . -p /dev/ttyUSB0 --fqbn 'arduino:avr:nano'
arduino-cli upload . -p /dev/ttyUSB0 --fqbn 'espressif:esp32:esp32s2'
arduino-cli upload . -p /dev/ttyUSB0 --fqbn 'esp32:esp32:esp32s2usb'
arduino-cli upload . -p /dev/ttyUSB0 --fqbn 'esp32:esp32:esp32wrover'
arduino-cli upload . -p /dev/ttyUSB0 --fqbn 'esp32:esp32:esp32doit-devkit-v1'
arduino-cli upload . -p /dev/ttyUSB0 --fqbn 'esp32:esp32:nodemcu-32s'
arduino-cli upload -p --fqbn esp32:esp32:nodemcu-32s .
arduino-cli upload -p /dev/ttyUSB0 --fqbn esp32:esp32:nodemcu-32s .
```

## arduino-cli lib install

```bash
arduino-cli lib install "Paperdink"
arduino-cli lib install "GxEPD2"
arduino-cli lib install "Avr"
arduino-cli lib install "avr"
arduino-cli lib install "AVR"
arduino-cli lib install "keyboard"
arduino-cli lib install "Adafruit SSD1306"
arduino-cli lib install "Adafruit_SSD1306"
arduino-cli lib install "Adafruit GFX Library"
arduino-cli lib install "Adafruit GFX"
arduino-cli lib install Adafruit_GFX
arduino-cli lib install "DEV_Config"
arduino-cli lib install "pgmspace"
arduino-cli lib install "SPI"
arduino-cli lib install "spi"
arduino-cli lib install "arduino"
arduino-cli lib install "Ntpclient"
arduino-cli lib install "Time"
arduino-cli lib install "NTP"
arduino-cli lib install "WiFiup"
arduino-cli lib install "WiFi"
arduino-cli lib install "Packetizer"
arduino-cli lib install "MsgPack"
arduino-cli lib install "ESP32Servo"
arduino-cli lib install --git-url https://github.com/collin80/esp32_can
arduino-cli lib install "ArxContainer"
arduino-cli lib install "ArduinoJson"
```
