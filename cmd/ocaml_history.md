```bash
history 'dune' --exact --max 1000 | xclip -selection clipboard
history 'dune' --max 10
dune exec bin/main.exe
dune exec main.exe
dune exec kash.exe
dune exec ./kash.exe
dune build
dune build bin/main.exe
dune init proj kash
opam install dune
dune exec src/proto_alpha/lib_protocol/test/integration/michelson/main.exe -- --file test_global_constants_storage.ml
dune exec src/proto_alpha/lib_protocol/test/unit/main.exe -- --file test_global_constants_storage.ml
```

```bash
eval (opam env)
opam init
opam install dune
opam update
opam switch create 5.1.1
opam switch list-available
opam switch --help
opam switch list
opam --version
sudo pacman -Ss opam
opam init --bare
sudo pacman -S opam
sudo pacman -S m4 build-essential patch unzip opam jq bc
```
