## bash

```bash
killall vivaldi-bin
killall telegram-desktop
killall signal-desktop
killall vivaldi
killall fcitx5
killall signal
kill -9 9963
killall obsidian
kill -9 1514
kill 1514
killall --help
killall nyxt
kill 3272
```

```bash
ps --help simple
ps --help output
ps --help
ps --help 1272
ps 1272
ps -A
ps -A | grep signal
ps -A | grep obsidian
ps -A | grep nyxt
```

```bash
history 'grep' --max 100
journalctl -k -b | grep usb
grep -ir 'jpg' ./
grep -ir 'background-image' ./*
grep -ir 'background-umage' ./*
grep -i 'background-umage' ./*
grep -i 'background-umage' ./
grep -ri 'bg_doodle' ../../*
grep -ri 'png' ./*
grep -ri 'bg_doodle' ./*
grep -ri 'bg_doodle' ./
grep -r "example" ./*
grep -r "Debug" ./*
grep -ri "rot" ./*
grep -ri "rotate" ./*
modprobe -c | grep btusb
grep -ri "YongKang" ../*
grep -ri "YongKang" ./*
arduino-cli board listall | grep 'nano'
arduino-cli board listall | grep 's2'
arduino-cli board listall | grep 'saola'
arduino-cli board listall esp32 | grep 'wrover'
arduino-cli board listall | grep 'nodemcu'
arduino-cli board listall esp32 | grep 'rover'
arduino-cli board listall esp32 | grep 's2'
arduino-cli board listall esp32 | grep 'w10'
arduino-cli board listall esp32 | grep 'saola'
grep -ri "cFont" ./*
grep -ri "config" ./data/src/*
grep 'config' ./data/src
ps -A | grep signal
pacmd list-sources | grep -e 'index:' -e device.string -e 'name:'
grep -ri "tzip/tzip" ./*
grep -ri "tezos/tezos" ./*
grep -i "tezos/tezos" ./*
grep -i "todo" ./*
grep -i "todo"
ps -A | grep obsidian
journalctl --since "2023-10-06" --until "2023-10-17" | grep -i "rotten"
journalctl --since "2023-10-06" --until "2023-10-17" | grep -i "error"
cat /var/log/syslog | grep -i "rotten"
timedatectl list-timezones | grep Taipei
timedatectl list-timezones | grep Paris
timedatectl list-timezones | grep Euro
timedatectl list-timezones | grep France
xev | grep keysym
sudo pacman -S ripgrep
ps -A | grep nyxt
lsmod | grep snd
lspci -v | grep -A7 i audio
lspci | grep i audio
```

```bash
journalctl -k -b | grep usb
brightnessctl m
brightnessctl g
brightnessctl g %
brightnessctl s 10%-
brightnessctl s -10%
brightnessctl s +10%
brightnessctl s +10
brightnessctl --help
sudo pacman -S brightnessctl
sudo pacman -Ss brightnessctl
systemctl start bluetooth.service
timedatectl status
timedatectl set-ntp true
systemctl start systemd-timesyncd
timedatectl
systemctl start lxdm.service
systemctl stop lxdm.service
systemctl stop/start lxdm.service
sudo systemctl restart NetworkManager
journalctl --since "2023-10-06" --until "2023-10-17" | grep -i "rotten"
journalctl --since "2023-10-06" --until "2023-10-17" | grep -i "error"
journalctl --since "today"
timedatectl set-timezone Asia/Taipei
timedatectl list-timezones | grep Taipei
timedatectl set-timezone Europe/Paris
timedatectl list-timezones | grep Paris
timedatectl list-timezones | grep Euro
timedatectl list-timezones | grep France
timedatectl list-timezones
systemctl status alsa-state.service
sudo localectl set-locale LANG="en_US.UTF-8"
```

```bash
sudo xinput set-prop 10 315 1
sudo chmod a+rw /dev/ttyUSB0
sudo pacman -Syu
sudo pacman -Ssy
sudo pacman -Ss cp210
sudo xinput set-prop 11 315 1
sudo pacman -S qt6
sudo pacman -Ss qt6
sudo pacman -Ss qt
sudo pacman -Sy telegram-desktop
sudo pacman -S brightnessctl
sudo pacman -Ss brightnessctl
sudo pacman -S light
sudo pacman -S bluez-utils
sudo pacman -Ss opam
sudo pacman -Ss dropbox
sudo ./dropboxd
sudo pacman -S bluez
sudo pacman -Ss bluez
sudo pacman -S blueman
sudo pacman -Ss blueman
sudo dmesg
sudo pacman -Ss showkey
sudo pacman -Ss telegram-desktop
sudo pacman -S ffmpeg
sudo pacman -S libvpx
sudo pacman -Ss libvpx
sudo pacman -S telegram-desktop
sudo pacman -Sy archlinux-keyring
sudo pacman -Rd telegram-desktop
sudo pacman -S libvpx1.3
sudo pacman -S abseil-cpp
sudo pacman -Ss abseil-cpp
sudo pacman -S signal-desktop
sudo pacman -S uboot-tools
sudo pacman -S uboot
sudo pacman -Ss uboot
sudo pacman -S gxmessage
sudo pacman -Ss gxmessage
sudo pacman -S xorg-xlsfonts
sudo pacman -S xlsfonts
sudo pacman -Ss xlsfonts
sudo pacman -S p7zip
sudo pacman -Ss 7z
sudo pacman -S picocom
sudo pacman -Ss picocom
sudo pacman -Ss coolterm
sudo chmod a+rw ./ttyUSB0
sudo usermod -aG dialout yunyan
sudo adduser yunyan dialout
sudo pacman -R brltty
sudo pacman -S usbutils
sudo pacman -Ss usbutils
sudo pacman -Fs lsusb
sudo pacman -Ss lsusb
sudo pacman -S screen
sudo pacman -S python-pyserial
sudo pacman -Ss python-pyserial
sudo pip3 install pyserial
sudo pacman -Ss arduino-cli
sudo pacman -Ss fuse2
sudo pacman -Ss fuse
sudo pacman -Ss libfuse
sudo pacman -Ss libfuse2
sudo pacman -S arduino arduino-cli arduino-builder
sudo pacman -Ss arduino
sudo pacman -Ss glibc
sudo pacman -S rpi-imager
sudo pacman -Ss rpi-imager
sudo pacman -Ss ail-cli
sudo chmod +x Todoist-linux-8.17.3-x86_64.AppImage
sudo pacman -Ss todoist
sudo mkdir backups
sudo tlmgr update --list
sudo pacman -S texlive
sudo pacman -S zlib
sudo pacman -Ss zlib
sudo pacman -Ss texlive
sudo pacman -S code
sudo pacman -S extra/code
sudo pacman -Ss extra/code
sudo pacman -Ss vscode
sudo pacman -Ss code
sudo hwclock --hctosys
sudo pacman -S openntpd
sudo pacman -S ntpd
sudo pacman -Ss ntpd
sudo pacman -Ss ntp
sudo macpan -Ss ntp
sudo macpan -Ss ntpd
sudo pacman -S opam
sudo pacman -Ss build-essential
sudo pacman -S build-essential
sudo pacman -S m4 build-essential patch unzip opam jq bc
sudo pacman -S npm
sudo pacman -S hugo
sudo pacman -Ss hugo
sudo npm --global remove npm
sudo pacman -Syy
sudo npm install -g serve
sudo pacman -Ss npm
sudo pacman -Ss npx
sudo rm -rf node_modules
sudo npm install npm@latest
sudo npm install -g npm@latest
sudo npm update -g tar
sudo npm install -g http-server
sudo npm install -g typescript
sudo pacman -Ss sumy
sudo pacman -Ss python-sumy
sudo pacman -S python-sumy
sudo pacman -S yay
sudo pacman -S python-nltk
sudo pacman -Ss python-pip
sudo pacman -Ss update-icon-caches
sudo update-icon-caches /usr/share/icons/*
sudo make install
sudo pacman -S fcitx5-material-color
sudo systemctl restart NetworkManager
sudo pacman -S obsidian
sudo pacman -Ss obsidian
sudo xinput set-prop 9 315 1
sudo pacman -Ss fcitx5-im
sudo pacman -R amtk
sudo chmod +x ./the_captain_1_1_2_58868.sh
sudo pacman -S glibc
sudo pacman -Ss signal
sudo pacman -S fuse2
sudo pacman -S jq
sudo pacman -Ss jq
sudo pacman -S asciinema
sudo pacman -Ss asciinema
sudo pacman -S mons
sudo pacman -Ss mons
sudo pacman -S xclip
sudo pacman -Ss xclip
sudo pacman -S spotify-launcher
sudo pacman -S spofity-launcher
sudo pacman -Ss spotify
sudo pacman -Ss ocaml
sudo pacman -S bookworm
sudo pacman -Ss Bookworm
sudo pacman -S python-pycryptodome
sudo pacman -S python-requests
sudo pacman -S python-colorama
sudo pacman -Ss colorama
sudo pacman -S python-pip
sudo pacman -S pip
sudo pacman -Ss pip
sudo pacman -S desmume
sudo pacman -S eesmume
sudo pacman -Ss DesmuME
sudo pacman -S xorg-xev
sudo pacman -Ss xev
sudo pacman-Ss xev
sudo pacman -S ripgrep
sudo pacman -S scrot
sudo chmod +s /usr/bin/light
sudo light -A 50
sudo light -U 50
sudo light -A 100
sudo light -U 100
sudo light -U 10
sudo light -S 100
sudo light -S 50
sudo light -s 50
sudo pacman -Ss light
sudomacpan -Ss light
sudo pacman -S acpilight
sudo pacman -Ss acpilight
sudo pacman -Ss acpiight
sudo killall --help
sudo kill -p 1514
sudo kill --help
sudo kill 1514
sudo /etc/environment
sudo pacman -S fcitx5-im
sudo pacman -S fcitx5-chewing
sudo pacman -S fcitx5-configtool
sudo pacman -S fcitx5 fcitx5-chewing
sudo pacman -Rs fcitx-configtool
sudo pacman -Rs fcitx
sudo pacman -S fcitx5 fcitx-chewing
sudo pacman -Ss fcitx5
sudo pacman -Ss telegram
sudo pacman -S nyxt
sudo pacman -Ss nyxt
sudo pacman -Rndd xdg-desktop-portal xdg-desktop-portal-gtk
sudo pacman -Qs portal
sudo pacman -S xorg-xbacklight
sudo pacman -S xbacklight
sudo rm ./20-intel.conf
sudo mv xorg.conf _xorg_conf
sudo pacman -S sof-firmware
sudo pacman -S alsa-utils
sudo pacman -Ss alsa-utils
sudo pacman -Ss pulseaudio-alsa
sudo pacman -S alsa-tools
sudo pacman -Ss alsa
sudo xinput set-prop 10 316 1
sudo pacman -S xorg-xinput
sudo pacman -Ss xinput
sudo pacman -Ss aac
sudo pacman -S openh264
sudo pacman -Ss h.264
sudo pacman -S vivaldi
sudo pacman -Ss vivaldi
sudo pacman -Ss fc-query
sudo pacman -Ss xft
sudo pacman -S powerline
sudo pacman -S xorg-xrandr
sudo pacman -Ss xorg-xbacklight
sudo pacman -S noto-fonts noto-fonts-emoji
sudo pacman -S noto-fonts-cjk
sudo pacman -Ss noto
sudo pacman -Ss viva
sudo pacman -Ss vivadi
sudo pacman -S fcitx fcitx-configtool
sudo pacman -Ss fcitx-im
sudo pacman -Ss fcitx
sudo pacman -S pamixer
sudo pacman -S xbacklight pamixer
sudo pacman -Ss pamixer
sudo pacman -S xorg-xmessage
sudo pacman -S gmrun dmenu
sudo pacman -Ss dmenu
sudo pacman -Ss dmenu-run
sudo pacman -Ss dmenu_run
sudo pacman -S stack
sudo pacman -Ss stack
sudo locale-gen
sudo emacs -nw /etc/locale.gen
sudo localectl set-locale LANG="en_US.UTF-8"
sudo pacman -Ss hostname
sudo pacman -Ss gnome-terminal
sudo pacman -Ss terminal
sudo pacman -S ttf-dejavu-nerd
sudo pacman -S ttf-dejavu
sudo pacman -Ss ttf-dejavu
sudo pacman -Ss dejavu
sudo pacman -S dejavu
```


```bash
sudo xinput set-prop 10 315 1
xinput list-props 10
xinput list-props 11
sudo xinput set-prop 11 315 1
xinput list-props 12
xinput list-props 13
xinput test-xi2 --root
xinput --help
xinput show
xinput list-props
xinput list-props 9
sudo xinput set-prop 9 315 1
xinput list
sudo xinput set-prop 10 316 1
sudo pacman -S xorg-xinput
sudo pacman -Ss xinput
```

```bash
sudo pacman -Syu
sudo pacman -Ssy
sudo pacman -Ss cp210
sudo pacman -S qt6
sudo pacman -Ss qt6
sudo pacman -Ss qt
sudo pacman -Sy telegram-desktop
sudo pacman -S brightnessctl
sudo pacman -Ss brightnessctl
sudo pacman -S light
sudo pacman -S bluez-utils
sudo pacman -Ss opam
sudo pacman -Ss dropbox
sudo pacman -S bluez
sudo pacman -Ss bluez
sudo pacman -S blueman
sudo pacman -Ss blueman
sudo pacman -Ss showkey
sudo pacman -Ss telegram-desktop
sudo pacman -S ffmpeg
sudo pacman -S libvpx
sudo pacman -Ss libvpx
sudo pacman -S telegram-desktop
sudo pacman -Sy archlinux-keyring
sudo pacman -Rd telegram-desktop
sudo pacman -S libvpx1.3
sudo pacman -S abseil-cpp
sudo pacman -Ss abseil-cpp
sudo pacman -S signal-desktop
sudo pacman -S uboot-tools
sudo pacman -S uboot
sudo pacman -Ss uboot
sudo pacman -S gxmessage
sudo pacman -Ss gxmessage
sudo pacman -S xorg-xlsfonts
sudo pacman -S xlsfonts
sudo pacman -Ss xlsfonts
sudo pacman -S p7zip
sudo pacman -Ss 7z
sudo pacman -S picocom
sudo pacman -Ss picocom
sudo pacman -Ss coolterm
sudo pacman -R brltty
sudo pacman -S usbutils
sudo pacman -Ss usbutils
sudo pacman -Fs lsusb
sudo pacman -Ss lsusb
sudo pacman -S screen
sudo pacman -S python-pyserial
sudo pacman -Ss python-pyserial
sudo pacman -Ss arduino-cli
sudo pacman -Ss fuse2
sudo pacman -Ss fuse
sudo pacman -Ss libfuse
sudo pacman -Ss libfuse2
sudo pacman -S arduino arduino-cli arduino-builder
sudo pacman -Ss arduino
sudo pacman -Ss glibc
sudo pacman -S rpi-imager
sudo pacman -Ss rpi-imager
sudo pacman -Ss ail-cli
sudo pacman -Ss todoist
sudo pacman -S texlive
sudo pacman -S zlib
sudo pacman -Ss zlib
sudo pacman -Ss texlive
sudo pacman -S code
sudo pacman -S extra/code
sudo pacman -Ss extra/code
sudo pacman -Ss vscode
sudo pacman -Ss code
sudo pacman -S openntpd
sudo pacman -S ntpd
sudo pacman -Ss ntpd
sudo pacman -Ss ntp
sudo pacman -S opam
sudo pacman -Ss build-essential
sudo pacman -S build-essential
sudo pacman -S m4 build-essential patch unzip opam jq bc
sudo pacman -S npm
sudo pacman -S hugo
sudo pacman -Ss hugo
sudo pacman -Syy
sudo pacman -Ss npm
sudo pacman -Ss npx
sudo pacman -Ss sumy
sudo pacman -Ss python-sumy
sudo pacman -S python-sumy
sudo pacman -S yay
sudo pacman -S python-nltk
sudo pacman -Ss python-pip
sudo pacman -Ss update-icon-caches
sudo pacman -S fcitx5-material-color
sudo pacman -S obsidian
sudo pacman -Ss obsidian
sudo pacman -Ss fcitx5-im
sudo pacman -R amtk
sudo pacman -S glibc
sudo pacman -Ss signal
sudo pacman -S fuse2
sudo pacman -S jq
sudo pacman -Ss jq
sudo pacman -S asciinema
sudo pacman -Ss asciinema
sudo pacman -S mons
sudo pacman -Ss mons
sudo pacman -S xclip
sudo pacman -Ss xclip
sudo pacman -S spotify-launcher
sudo pacman -S spofity-launcher
sudo pacman -Ss spotify
sudo pacman -Ss ocaml
sudo pacman -S bookworm
sudo pacman -Ss Bookworm
sudo pacman -S python-pycryptodome
sudo pacman -S python-requests
sudo pacman -S python-colorama
sudo pacman -Ss colorama
sudo pacman -S python-pip
sudo pacman -S pip
sudo pacman -Ss pip
sudo pacman -S desmume
sudo pacman -S eesmume
sudo pacman -Ss DesmuME
sudo pacman -S xorg-xev
sudo pacman -Ss xev
sudo pacman-Ss xev
sudo pacman -S ripgrep
sudo pacman -S scrot
sudo pacman -Ss light
sudo pacman -S acpilight
sudo pacman -Ss acpilight
sudo pacman -Ss acpiight
sudo pacman -S fcitx5-im
sudo pacman -S fcitx5-chewing
sudo pacman -S fcitx5-configtool
sudo pacman -S fcitx5 fcitx5-chewing
sudo pacman -Rs fcitx-configtool
sudo pacman -Rs fcitx
sudo pacman -S fcitx5 fcitx-chewing
sudo pacman -Ss fcitx5
sudo pacman -Ss telegram
sudo pacman -S nyxt
sudo pacman -Ss nyxt
sudo pacman -Rndd xdg-desktop-portal xdg-desktop-portal-gtk
sudo pacman -Qs portal
sudo pacman -S xorg-xbacklight
sudo pacman -S xbacklight
sudo pacman -S sof-firmware
sudo pacman -S alsa-utils
sudo pacman -Ss alsa-utils
sudo pacman -Ss pulseaudio-alsa
sudo pacman -S alsa-tools
sudo pacman -Ss alsa
sudo pacman -S xorg-xinput
sudo pacman -Ss xinput
sudo pacman -Ss aac
sudo pacman -S openh264
sudo pacman -Ss h.264
sudo pacman -S vivaldi
sudo pacman -Ss vivaldi
sudo pacman -Ss fc-query
sudo pacman -Ss xft
sudo pacman -S powerline
sudo pacman -S xorg-xrandr
sudo pacman -Ss xorg-xbacklight
sudo pacman -S noto-fonts noto-fonts-emoji
sudo pacman -S noto-fonts-cjk
sudo pacman -Ss noto
sudo pacman -Ss viva
sudo pacman -Ss vivadi
sudo pacman -S fcitx fcitx-configtool
sudo pacman -Ss fcitx-im
sudo pacman -Ss fcitx
sudo pacman -S pamixer
sudo pacman -S xbacklight pamixer
sudo pacman -Ss pamixer
sudo pacman -S xorg-xmessage
sudo pacman -S gmrun dmenu
sudo pacman -Ss dmenu
sudo pacman -Ss dmenu-run
sudo pacman -Ss dmenu_run
sudo pacman -S stack
sudo pacman -Ss stack
sudo pacman -Ss hostname
sudo pacman -Ss gnome-terminal
sudo pacman -Ss terminal
sudo pacman -S ttf-dejavu-nerd
sudo pacman -S ttf-dejavu
sudo pacman -Ss ttf-dejavu
sudo pacman -Ss dejavu
sudo pacman -S dejavu
```

```bash
du -h --max-depth=3 ./Downloads/
du -h --max-depth=3 ./
du -h --max-depth=1 ./
du -h --max-depth=1 ~/
du -h --max-depth=1 /
du -h --max-depth=1

```

## fish


### COmmand history

Notice: `xclip` is required.

```bash
history 'arduino-cli core' --max 100 | xclip -selection clipboard
```

