import XMonad
import XMonad.Util.SpawnOnce
import XMonad.Util.Run
import XMonad.Hooks.ManageDocks
import XMonad.Util.EZConfig
import XMonad.Actions.CycleWS
import XMonad.Hooks.DynamicLog
import XMonad.Layout.ThreeColumns

import Data.Monoid
import System.Exit

import qualified XMonad.StackSet as W
import qualified Data.Map        as M

-- The preferred terminal program, which is used in a binding below and by
-- certain contrib modules.
--
myTerminal      = "gnome-terminal"

--
myFocusFollowsMouse  = False
myClickJustFocuses   = True
myBorderWidth        = 8
myNormalBorderColor  = "#775555"
myFocusedBorderColor = "#00ff55"

--
myModMask            = mod4Mask

myWorkspaces         = map show [1..9]
-- myWorkspaces         = ["main", "im", "web" ] ++ map show [4..6]
--

selectedPrintScreen =
  "scrot -s '/tmp/%F_%T_$wx$h.png' -e " ++
  "'xclip -selection clipboard -target image/png -i $f'"

-- ####################################################################

myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $

    -- ## launch a terminal
    [ ((modm .|. shiftMask, xK_Return), spawn $ XMonad.terminal conf)

    -- ## launch browser
    --  , ((modm .|. shiftMask, xK_backslash)
    --  , spawn "qutebrowser --qt-flag disable-gpu")

    -- ## launch dmenu/gmrun
    , ((modm,               xK_p     ), spawn "dmenu_run")
    , ((modm .|. shiftMask, xK_p     ), spawn "gmrun")

    -- ## Toggle the status bar gap
    , ((modm              , xK_b     ), sendMessage ToggleStruts)

    -- ## close focused window
    , ((modm .|. shiftMask, xK_c     ), kill)

    -- ## Quit xmonad
    , ((modm .|. shiftMask, xK_q     ), io (exitWith ExitSuccess))

    -- ## Restart xmonad
    , ((modm              , xK_q     )
      ,spawn "xmonad --recompile; xmonad --restart")

    -- ## Run xmessage to show keybindings
    -- ## hint: pacman -S gxmessage
    , ((modm .|. shiftMask, xK_h )
      ,spawn ("echo \"" ++ hint ++ "\" | " ++ helpcmd))
    , ((modm              , xK_h )
      ,spawn ("echo \"" ++ hint ++ "\" | " ++ helpcmd))
    , ((modm .|. shiftMask, xK_slash )
      ,spawn ("echo \"" ++ help ++ "\" | " ++ helpcmd))
    -- ###### LAYOUT #####

    -- ## Rotate through the available layout algorithms
    , ((modm,               xK_space ), sendMessage NextLayout)

    -- ## Swap the focused window
    , ((modm,               xK_Return), windows W.swapMaster)
    -- , ((modm .|. shiftMask, xK_k     ), windows W.swapDown  )
    -- , ((modm .|. shiftMask, xK_j     ), windows W.swapUp    )

    -- Shink/Expand the master area
    , ((modm,               xK_a     ), sendMessage Shrink)
    , ((modm,               xK_d     ), sendMessage Expand)

    -- +/- the number of windows in the master area
    , ((modm              , xK_comma ), sendMessage (IncMasterN 1))
    , ((modm              , xK_period), sendMessage (IncMasterN (-1)))

    -- ##### MOVE FOCUS #####

    -- ## Move focus to the next window
    , ((modm,               xK_Tab   ), windows W.focusDown)
    , ((modm .|. shiftMask, xK_Tab   ), windows W.focusUp)
    , ((modm,               xK_k     ), windows W.focusDown)
    , ((modm,               xK_j     ), windows W.focusUp  )
    , ((modm,               xK_m     ), windows W.focusMaster  )

    -- ## Move focus to next workspace
    , ((modm              , xK_w     ), prevWS)
    , ((modm              , xK_s     ), nextWS)
    ]
    ++

    --
    -- mod-[1..9], Switch to workspace N
    -- mod-shift-[1..9], Move client to workspace N
    --
    [ ((m .|. modm, k), windows $ f i)
    | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
    , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    ++

    -- selective screentshot
    [((modm, xK_Print), spawn selectedPrintScreen)]
    ++

    --
    -- mod-{e, r, t}, Switch to physical/Xinerama screens 1, 2, or 3
    -- mod-shift-{e, r, t}, Move client to screen 1, 2, or 3
    --
    [ ((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
    | (key, sc) <- zip [xK_e, xK_r, xK_t] [0..]
    , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]

-- ####################################################################

ezkeys =
    -- volume control
    [ ("<XF86AudioLowerVolume>"     , spawn "pamixer -d 5")
    , ("<XF86AudioRaiseVolume>"     , spawn "pamixer -i 5")
    , ("<XF86AudioMute>"            , spawn "pamixer -t")
    -- monitor/screen light
    , ("<XF86MonBrightnessUp>"   , spawn "light -A 10")
    , ("<XF86MonBrightnessDown>" , spawn "light -U 10")
    -- keyboard backlight
    , ("<XF86KbdLightOnOff>"
      , spawn "echo \"Sorry, XF86XK_KbdLightOnOff hasn't been config yet\"")
    , ("<XF86KbdBrightnessUp>"
      , spawn "echo \"Sorry, XF86XK_KbdBrightnessUp hasn't been config yet\"")
    , ("<XF86KbdBrightnessDown>"
      , spawn "echo \"Sorry, XF86XK_KbdBrightnessDown hasn't been config yet\"")
    ]

-- ####################################################################

myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))
    ]

-- ####################################################################

myLayout = avoidStruts (tiled ||| Mirror tiled ||| Full ||| threeCol)
  where
     tiled   = Tall nmaster delta ratio
     threeCol = ThreeColMid nmaster delta ratio
     nmaster = 1
     ratio   = 1/2
     delta   = 3/100

-- ####################################################################

myManageHook = composeAll
    [ className =? "MPlayer"        --> doFloat
    , className =? "Gimp"           --> doFloat ]

-- ####################################################################

myEventHook = mempty

-- ####################################################################

-- myLogHook = return ()

myLogHook h = dynamicLogWithPP $ def
    { ppCurrent = wrap "<fc=#b8473d>[</fc><fc=#7cac7a>" "</fc><fc=#b8473d>]</fc>"
    , ppLayout = layoutHandler
    , ppTitle = \_ -> ""
    , ppOutput = hPutStrLn h
    }

layoutHandler :: String -> String
layoutHandler "Full" = "<fc=#ccc,#b8473d><F></fc>"
layoutHandler "Tall" = "<fc=#ccc,#3d47b8><T></fc>"
layoutHandler "Mirror Tall" = "<MT>"
layoutHandler "ThreeCol" = "<3T>"
layoutHandler a = "<"++a++">"

-- ####################################################################

myStartupHook = do
    spawnOnce "nitrogen --restore &"
    spawnOnce "picom &"
    spawnOnce "xrandr --output eDP-1 --brightness 0.75 &"
    spawnOnce "fcitx5 &"
    spawnOnce "mons -a &"

-- ####################################################################

--
main = do
    xmproc <- spawnPipe "xmobar -x 0 ~/.xmobarrc"
    xmonad $ docks $ defaults xmproc

-- ####################################################################

--
defaults xmproc = def {
      -- simple stuff
        terminal           = myTerminal,
        focusFollowsMouse  = myFocusFollowsMouse,
        clickJustFocuses   = myClickJustFocuses,
        borderWidth        = myBorderWidth,
        modMask            = myModMask,
        workspaces         = myWorkspaces,
        normalBorderColor  = myNormalBorderColor,
        focusedBorderColor = myFocusedBorderColor,

      -- key bindings
        keys               = myKeys,
        mouseBindings      = myMouseBindings,

      -- hooks, layouts
        layoutHook         = myLayout,
        manageHook         = myManageHook,
        handleEventHook    = myEventHook,
        logHook            = myLogHook xmproc,
        startupHook        = myStartupHook
    } `additionalKeysP` ezkeys

helpcmd :: String
helpcmd = "gxmessage -fg \"#C4A000\" -bg \"#171421\" -font 'mono 12' -file -"
-- Finally, a copy of the default bindings in simple textual tabular format.
help :: String
help = unlines [
    "The default modifier key is 'win'. Default keybindings:",
    "",
    "",
    "### PROGRAM ###",
    "",
    "mod-Shift-Enter  Launch xterminal",
    "mod-p            Launch dmenu",
    "mod-Shift-p      Launch gmrun",
    "mod-b            Toggle xmobar",
    "",
    "-- DANGER --",
    "mod-Shift-c      kill the focused window",
    "mod-q            Restart xmonad",
    "mod-Shift-q      Quit xmonad",
    "",
    "### LAYOUT ###",
    "",
    "mod-Space        Rotate through the available layout algorithms",
    "mod-a            Shrink the master area",
    "mod-d            Expand the master area",
    "mod-,            Increment the number of windows in the master area",
    "mod-.            Decrement the number of windows in the master area",
    "",
    "-- move focused window ",
    "mod-Return         Swap to master",
    "mod-Shift-[1..9]   Move to workspace N",
    "",
    "### MOVE FOCUS  ###",
    "",
    "mod-Tab          Move focus to the next window",
    "mod-Shift-Tab    Move focus to the previous window",
    "mod-k            Move focus to the next window",
    "mod-j            Move focus to the previous window",
    "mod-m            Move focus to the master window",
    "",
    "mod-w            Move focus to the previous workspace",
    "mod-s            Move focus to the next workspace",
    "mod-[1..9]       Move focus to workSpace N",
    "",
    "### MISC. ###",
    "",
    "-- Mouse bindings: default actions bound to mouse events",
    "mod-button1  Set the window to floating mode and move by dragging",
    "mod-button2  Raise the window to the top of the stack",
    "mod-button3  Set the window to floating mode and resize by dragging"]

hint :: String
hint = unlines [
    "# === Hint === #",
    "",
    "## --- fcitx ---  ##",
    "",
    "### ... quick phrase ... ###",
    "    super + :",
    "    -------------------------",
    "    phrase can be found in ~/.config/fcitx/data/*.mb",
    "",
    "...",
    --
    "",
    ".. press ECS to exit.",
    ""]
