;; --------------------
;; === global setup ===
;; --------------------
(set-language-environment 'utf-8)
(set-default-coding-systems 'utf-8)
(set-file-name-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(prefer-coding-system 'utf-8)
;;
(setq auto-save-default nil)
(setq create-lockfiles nil)
(setq delete-auto-save-files t)
(setq make-backup-files nil)
(setq x-select-enable-clipboard t)
;; --------------------
(require 'package)
(setq package-archives
'(
  ("melpa-stable" . "https://stable.melpa.org/packages/")
  ("melpa" . "https://melpa.org/packages/")
  ("org" . "https://orgmode.org/elpa/")
  ("gnu" . "https://elpa.gnu.org/packages/")
))
(package-initialize)
;; --------------------
(unless package-archive-contents
  (package-refresh-contents))
(setq my-package-list '(use-package))
(dolist (package my-package-list)
  (unless (package-installed-p package)
    (package-install package)))
;; --------------------
(show-paren-mode 1)
(xclip-mode 1)
(column-number-mode 1)
(electric-pair-mode 1)
(electric-indent-mode +1)
(global-set-key (kbd "RET") 'newline-and-indent)
(global-set-key (kbd "C-RET") 'newline)
(setq-default electric-indent-inhibit t)
;; --------------------
(setq tab-width 4)
;;
;; (setq indent-tabs-mode nil)
;; (defun disable-tabs ()
;;   (setq indent-tabs-mode nil))
;; (defun enable-tabs  ()
;;   (local-set-key (kbd "TAB") 'tab-to-tab-stop)
;;   (setq indent-tabs-mode t)
;;   (setq tab-width custom-tab-width))
;;
;; ----------------------------
;; === custom aux functions ===
;; ----------------------------
;;
(defun show-file-name ()
  "Show the full path file name in the minibuffer."
  (interactive)
  (message (buffer-file-name)))
(global-set-key (kbd "C-c C-f") 'show-file-name)
(global-set-key (kbd "C-x <up>") 'beginning-of-buffer)
(global-set-key (kbd "C-x <down>") 'end-of-buffer)
;;
;;
;; --------------
;; === cl-lib ===
;; --------------
;;
(setq byte-complile-warnings '(not cl-functions))

(require 'cl-lib)
;; (use-package 'loadhist)
;; (require 'loadhist)
;; (file-dependents (feature-file 'cl))
;; (use-package cl-lib
;;   :ensure t
;; )
;;
;; ---------------------
;; === unicode-fonts ===
;; ---------------------
;;
(use-package unicode-fonts
  :ensure t
  :config (unicode-fonts-setup)
)
;; (setq my-font "DejaVu Sans Mono-10")
;; (add-to-list 'default-frame-alist '(font . "DejaVu Sans Mono-20" ))
;; (set-face-attribute 'default t :font "DejaVu Sans Mono-20" )
;; (add-to-list 'default-frame-alist '(font . "DejaVu Sans Mono-10"))
;; (set-default-font "DejaVuSansMono 12")
;;
;; -------------------
;; === color-theme ===
;; -------------------
;;
(use-package color-theme-modern
  :ensure t
  :config
  (progn
    (setq color-theme-is-global t)
    (load-theme 'desert t t)
    (enable-theme 'desert))
)
;;
;; ------------------
;; === whitespace ===
;; ------------------
;;
(require 'whitespace)
(setq whitespace-style
      '(face
       ; indentation
       trailing
       lines
       ; empty
       spaces
       ; space-mark
       space-before-tab
       space-after-tab
       tabs
       tab-mark
       ; newline
       ; newline-mark
       ))
(global-whitespace-mode 1)
;;
;; ------------------------------
;; === bm (visible bookmarks) ===
;; ------------------------------
;;
;; in file bookmark
;;
;; (use-package bm
;;   :ensure t
;;   :bind
;;   ("C-x B" . 'bm-toggle)
;;   ("C-x j <right> " . 'bm-next)
;;   ("C-x j <left>" . 'bm-previous)
;; )
;;
;; -------------------
;; === rg (ripgrep) ===
;; -------------------
;;
(use-package rg
  :ensure t
  :config (rg-enable-default-bindings)
)
;;
;; -----------------
;; === xah-dired ===
;; -----------------
;;
;; directory-based search and replace
;; sohuld be able to be replace by rg-mode
;;
;; (use-package xah-find
;;   :ensure t
;;   :bind
;;   ("C-x x f" . xah-find-text)
;;   ("C-x x F" . xah-find-text-regex)
;;   ("C-x x r" . xah-find-replace-text)
;;   ("C-x x R" . xah-find-replace-text-regex)
;; )
;;
;; ------------------
;; === find-dired ===
;; ------------------
;;
;; search file name
;;
(use-package find-dired
  :ensure t
  :bind
  ("C-f" . find-name-dired)
  :config
  (progn
    (setq find-ls-option '("-print0 | xargs -0 ls -log" . ""))
  )
)
;;
;; ---------------
;; === ibuffer ===
;; ---------------
;;
(use-package ibuffer
  :ensure t
  :bind ("C-x C-b" . ibuffer)
  :init
  (progn
    (require 'ibuf-ext)
    (setq ibuffer-filter-group-name-face 'success)
    (setq ibuffer-show-empty-filter-groups nil)
    ;; (add-to-list 'ibuffer-never-show-predicates "^\\*")
    (add-to-list 'ibuffer-never-show-predicates "~$")
    (add-hook 'ibuffer-mode-hooks 'ibuffer-auto-mode)
    (setq ibuffer-formats
          '((mark modified read-only " "
            (name 18 18 :left :elide) " "
            (size 9 -1 :right) " "
            (mode 16 16 :left :elide) " " filename-and-process)
            (mark " " (name 16 -1) " " filename))))
  :config
  (progn
    (setq ibuffer-default-sorting-mode 'major-mode))
)
;;
;; ----------------
;; === avy-mode ===
;; ----------------
;;
;; for finding text pattern in current window
;;
(use-package avy
  :ensure t
  :bind
  ("C-x a 1" . 'avy-goto-char)
  ("C-x a 2" . 'avy-goto-char-2)
  ("C-x a `" . 'avy-goto-line)
  ("C-x a w `" . 'avy-goto-word-0)
  ("C-x a w 1" . 'avy-goto-word-1)
  :config
  (progn
    (setq avy-keys (nconc (number-sequence ?1 ?9) (number-sequence ?a ?z)))
  )
)
;;
;; ---------------
;; === origami ===
;; ---------------
;;
;; text folding
;;
(use-package origami
  :ensure t
  :demand
  :config
  (define-prefix-command 'origami-mode-map)
  (global-set-key (kbd "C-x C-p") 'origami-mode-map)
  (global-origami-mode)
  :bind
  (:map origami-mode-map
   ("o" . origami-open-node)
   ("O" . origami-open-node-recursively)
   ("c" . origami-close-node)
   ("C" . origami-close-node-recursively)
   ("a" . origami-toggle-node)
   ("A" . origami-recursively-toggle-node)
   ("R" . origami-open-all-nodes)
   ("M" . origami-close-all-nodes)
   ("v" . origami-show-only-node)
   ("k" . origami-previous-fold)
   ("j" . origami-forward-fold)
   ("x" . origami-reset)))
;;
;; ------------------
;; === display-line-numbers-mode ===
;; ------------------
;;
;
(use-package display-line-numbers
  :ensure t
  :bind
  ("C-x |" . display-line-numbers-mode)
  :config
  (progn
    (global-display-line-numbers-mode -1)
    (setq display-line-numbers-format "%4d | ")
    (setq display-line-numbers-foreground "#666666")
    (whitespace-mode 0)
  )
)
;;
;; ----------------
;; === neo-tree ===
;; ----------------
;;
(use-package neotree
  :ensure t
  :commands neotree-toggle
  :bind ("C-x \\" . neotree-toggle)
  :config
  (progn
    ;; (use-package all-the-icons :ensure t)
    (setq neo-theme (if (display-graphic-p) 'icons 'arrow))
  )
)
;;
;; -----------------
;; === evil-mode ===
;; -----------------
;;
(use-package evil
  :ensure t
  :init
  (setq evil-default-state 'emacs)
  :config
  (progn
    (defun setup-modeline-green ()
      (set-face-foreground 'mode-line "#99FF99")
      (set-face-background 'mode-line "#009944")
      (set-face-foreground 'mode-line-inactive "#99FF99")
      (set-face-background 'mode-line-inactive "#242424")
      )
    ;;
    (defun setup-modeline-white ()
      (set-face-foreground 'mode-line "#ffffff")
      (set-face-background 'mode-line "#666666")
      (set-face-foreground 'mode-line-inactive "#ffffff")
      (set-face-background 'mode-line-inactive "#242424")
      )
    ;;
    (defun setup-modeline-dark ()
      (set-face-foreground 'mode-line "#555555")
      (set-face-background 'mode-line "#CCCCCC")
      (set-face-foreground 'mode-line-inactive "#555555")
      (set-face-background 'mode-line-inactive "#242424")
      )
    ;;
    (defun setup-modeline-yellow ()
      (set-face-foreground 'mode-line "#FFFF99")
      (set-face-background 'mode-line "#CC9900")
      (set-face-foreground 'mode-line-inactive "#FFFF99")
      (set-face-background 'mode-line-inactive "#242424")
      )
    ;;
    (defun setup-modeline-red ()
      (set-face-foreground 'mode-line "#FF9999")
      (set-face-background 'mode-line "#990000")
      (set-face-foreground 'mode-line-inactive "#FF9999")
      (set-face-background 'mode-line-inactive "#242424")
      )
    ;;
    (defun setup-modeline-blue ()
      (set-face-foreground 'mode-line "#AAAAFF")
      (set-face-background 'mode-line "#005588")
      (set-face-foreground 'mode-line-inactive "#AAAAFF")
      (set-face-background 'mode-line-inactive "#242424")
      )
    ;;
    (defun update-modeline-color()
      (cond
       ((memq evil-state '(emacs))  (setup-modeline-green)) ;; (hybrid x y)
       ((memq evil-state '(normal)) (setup-modeline-blue))
       ((memq evil-state '(insert)) (setup-modeline-yellow))
       ((memq evil-state '(visual)) (setup-modeline-red))
       (t                           (setup-modeline-white))
      ))
    ;;
    (add-hook 'post-command-hook         'update-modeline-color)
    (add-hook 'find-file-hook            'update-modeline-color)
    (add-hook 'windmove-do-window-select 'update-modeline-color)
    (evil-mode 1)
  )
)
;;
;; ---------------------
;; === auto-complete ===
;; ---------------------
;;
(use-package auto-complete
  :ensure t
  :commands auto-complete-mode
  :init
  (setq ac-auto-start 0
        ac-quick-help-delay 0.5
        ac-fuzzy-enable t
        ac-use-fuzzy t
        ac-auto-show-menu 0.2)
  :config
  (progn
    (ac-set-trigger-key "TAB")
  )
)
;;
;; ---------------------
;; === markdown mode ===
;; ---------------------
;;
(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown")
  :config (progn
            (add-hook 'markdown-mode-hook 'display-line-numbers-mode)
            (add-hook 'markdown-mode-hook 'whitespace-mode))
)
;;
;;
;; ----------------
;; === org mode ===
;; ----------------
;;
(use-package org
  :ensure t
  :init
  (progn
    (require 'ox-publish)
    (define-key global-map (kbd "C-c c") 'org-capture)
    (define-key org-mode-map (kbd "C-c C-a") 'org-agenda)
    (define-key org-mode-map (kbd "C-<tab>") nil)
    )
  :config
  (progn
    (setq org-hide-emphasis-markers nil)
    (setq org-todo-keywords
      '((sequence "TODO" "WIP" "|" "DONE")))
    (setf org-todo-keyword-faces
      '(("TODO" . (:foreground "white" :background "#95A5A6" :weight bold))
        ("WIP"  . (:foreground "white" :background "#2E8B57" :weight bold))
        ("DONE" . (:foreground "white" :background "#3498DB" :weight bold))))
    (add-hook 'org-mode-hook
             (lambda ()
                (whitespace-mode)
                (display-line-numbers-mode)))
  )
)
;;
;; --------------------
;; === haskell mode ===
;; --------------------
;;
(use-package haskell-mode
  :ensure t
  :config
    (progn
      (add-hook 'haskell-mode-hook 'whitespace-mode)
      (add-hook 'haskell-mode-hook 'display-line-numbers-mode)
    )
)
;;
;; -----------------------
;; === typescript mode ===
;; -----------------------
;;
;; Install use-package if not installed
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; Install and configure typescript-mode
(use-package typescript-mode
  :ensure t
  :config
    (setq-default typescript-indent-level 2) ;
    (setq-default indent-tabs-mode nil)      ;
  :mode "\\.ts\\'")
;;
;; -----------------------
;; === arduino mode ===
;; -----------------------

(use-package arduino-mode
  :ensure t
  :config
    (setq-default indent-tabs-mode nil);
  :mode "\\.ino\\'")

;; (add-to-list 'auto-mode-alist '("\\.ino$" . arduino-mode))
;; ------------------
;; === ocaml mode ===
;; ------------------
;;
;; ;; (use-package tuareg
;; ;;   :ensure t
;; ;;   :commands tuareg-mode
;; ;;   :config
;; ;;   (progn
;; ;;     ;; Add opam emacs directory to the load-path
;; ;;     (setq opam-share
;; ;;       (substring
;; ;;         (shell-command-to-string "opam config var share 2> /dev/null") 0 -1))
;; ;;     (add-to-list 'load-path (concat opam-share "/emacs/site-lisp"))
;; ;;     ;; Setup environment variables using OPAM
;; ;;     (dolist (var (car (read-from-string
;; ;;       (shell-command-to-string "opam config env --sexp"))))
;; ;;       (setenv (car var) (cadr var)))
;; ;;     ;; One of the `opam config env` variables is PATH. Update `exec-path` to that.
;; ;;     (setq exec-path (split-string (getenv "PATH") path-separator))
;; ;;     ;; Load merlin-mode
;; ;;     (require 'merlin)
;; ;;     (require 'ocp-indent)
;; ;;     ;; Use opam switch to lookup ocamlmerlin binary
;; ;;     (setq merlin-command 'opam)
;; ;;     (setq merlin-use-auto-complete-mode 'easy)
;; ;;     ;; Automatically load utop.el.
;; ;;     ; (add-to-list 'load-path "/path/to/utop/src/top")
;; ;;     (autoload 'utop-minor-mode "utop" "Minor mode for utop" t)
;; ;;     ;;
;; ;;     (define-key merlin-mode-map (kbd "M-<tab>") 'merlin-try-completion)
;; ;;     (define-key merlin-mode-map (kbd "C-c C-c") 'merlin-locate)
;; ;;     (define-key merlin-mode-map (kbd "C-c C-p") 'prev-match)
;; ;;     (define-key merlin-mode-map (kbd "C-c C-n") 'next-match)
;; ;;     ;; (define-key tuareg-mode-map (kbd "C-x C-r") 'tuareg-eval-region)
;; ;;     ;;
;; ;;     (setq merlin-logfile "~/merlin.log")
;; ;;     (setq merlin-error-after-save t)
;; ;;     (add-hook 'tuareg-mode-hook 'display-line-numbers-mode)
;; ;;     (add-hook 'tuareg-mode-hook 'whitespace-mode)
;; ;;     (add-hook 'tuareg-mode-hook 'merlin-mode)
;; ;;     (add-hook 'tuareg-mode-hook 
;; ;;         (lambda ()
;; ;;           (utop-minor-mode)
;; ;;           (define-key utop-minor-mode-map (kbd "C-c C-z") 'utop)
;; ;;           (setq indent-line-function 'ocp-indent-line)
;; ;;         )
;; ;;     )
;; ;;   )
;; ;; )
;;
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(tuareg haskell-mode markdown-mode auto-complete evil neotree origami avy rg color-theme-modern unicode-fonts xclip use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
