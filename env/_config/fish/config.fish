
# setting
set -x LANGUAGE en_US.UTF-8
set -x LANG en_US.UTF-8
set -x LC_ALL en_US.UTF-8
set -x LC_CTYPE zh_TW.UTF-8
set -x LESSCHARSET utf-8

set -x GTK_IM_MODULE fcitx
set -x QT_IM_MODULE fcitx
set -x XMODIFIERS @im=fcitx

# setting for PATH
set -x PATH /usr/bin $PATH
set -x PATH /usr/local/bin $PATH
set -x PATH ~/.cargo/bin $PATH
set -x PATH ~/.local/bin $PATH
set -x PATH ~/3rd-party/bin $PATH

# config for bobthefish
set -g theme_powerline_fonts yes
set -g theme_nerd_fonts no
set -g theme_title_display_path yes
set -g theme_color_scheme dark
set -g theme_date_format "+%H:%M"

# config functions
function confish
    emacs -nw ~/.config/fish/config.fish
end

function config-fish
    emacs -nw ~/.config/fish/config.fish
end

function config-git
    emacs -nw ~/.gitconfig
end

function config-xmonad
    emacs -nw ~/.xmonad/xmonad.hs
end

function config-xmobar
    emacs -nw ~/.xmobarrc
end

function config-picom
    emacs -nw ~/.config/picom/picom.conf
end

function ed
    emacs -nw $argv
end

function sued
    sudo emacs -nw $argv
end

if status is-interactive
    # Commands to run in interactive sessions can go here
end
