#!/usr/bin/env stack
-- stack --resolver lts-17.1 script
--
{-# LANGUAGE OverloadedStrings #-}
--
-- emacs
-- xmonad
-- xmobar
--
import Turtle
import System.Console.ANSI
import Control.Monad
--
import Data.Time.LocalTime
import Data.Time.Clock
import Data.Time.Format
--

--
checkBackupFolder :: IO Bool
checkBackupFolder = do
  _temp <- pwd
  _home <- home
  cd _home
  b1 <- testdir ".workingrc"
  if b1
  then do
    cd ".workingrc"
    flag <- testdir "backup"
    cd _temp
    return flag
  else do
    cd _temp
    return False
--
printInfo str = do
  setSGR [ SetColor Foreground Vivid Cyan ]
  putStr "[info] "
  setSGR [ SetColor Foreground Vivid White ]
  putStrLn str
--
printError str = do
  setSGR [ SetColor Foreground Vivid Red ]
  putStr "[error] "
  setSGR [ SetColor Foreground Vivid White ]
  putStrLn str
--
printWarning str = do
  setSGR [ SetColor Foreground Vivid Yellow ]
  putStr "[warning] "
  setSGR [ SetColor Foreground Vivid White ]
  putStrLn str
--
getLocalTime = do
  lz <- getCurrentTimeZone
  utc <- getCurrentTime
  return $ formatTime defaultTimeLocale "%Y-%m-%d_%H_%M"
         $ utcToLocalTime lz utc
--
parseLocalTime :: String -> LocalTime
parseLocalTime str =
  parseTimeOrError True
    defaultTimeLocale
    "%Y-%m-%d_%H_%M" str
--
main = do
  printInfo "checking required information.."
  pathHome <- home
  pathProj <- pwd
  --
  cd pathProj
  p0 <- testdir "env"
  if p0
    then do
      printInfo "OK"
    else do
      printError $ "Required folder is absent. Please re-download workingrc."
      exit (ExitFailure 1)
  --
  cd pathProj
  let source_emacs = pathProj </> (fromText "env/_emacs")
  let target_emacs = pathHome </> (fromText "_emacs")
  cp source_emacs target_emacs
  printInfo ".emacs is installed"
  cd pathProj
  --
  -- dd <- getLocalTime
  -- printInfo $ dd
  -- print $ parseLocalTime dd
  --
  --
  printInfo "installation finished"
