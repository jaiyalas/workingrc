# workingrc

- emacs
- git
- xmonad
- xmobar
- picom

## update

update local configs to this repo:

```
> ./update.sh
```

## Misc

### natural scroll

Using `xorg-xlist` can do the job.

`xinput`
