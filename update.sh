#!/bin/bash

echo -e "copying \e[32mfish\e[39m config file.."
cp ~/.config/fish/config.fish ./env/_config/fish/config.fish

echo -e "copying \e[32mpicom\e[39m config file.."
cp ~/.config/picom/picom.conf ./env/_config/picom/picom.conf

echo -e "copying \e[32mxmobar\e[39m config file.."
cp ~/.xmobarrc ./env/_xmobarrc

echo -e "copying \e[32mxmonad\e[39m config file.."
cp ~/.xmonad/xmonad.hs ./env/_xmonad/xmonad.hs

echo -e "copying \e[32memacs\e[39m config file.."
cp ~/.emacs ./env/_emacs

echo -e "copying \e[32mgit\e[39m config file.."
cp ~/.gitconfig ./env/_gitconfig
cp ~/.gitignore_global ./env/_gitignore_global

echo -e "copying \e[32mxorg\e[39m config file.."
cp ~/.xprofile ./env/_xprofile
cp ~/.xinitrc ./env/_xinitrc

echo -e "..\e[34mupdate completed\e[39m!"
